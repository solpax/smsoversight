package com.solpax.smsoversight;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class Alarms {
    /**
     * Schedule a sync right after an SMS arrived.
     */
    static void scheduleIncomingSync(Context ctx) {
        scheduleSync(ctx, 1);
    }

    /**
     * Schedule a sync 10 minutes for syncing outgoing SMS.
     */
    static void scheduleRegularSync(Context ctx) {
        scheduleSync(ctx, 600);
    }

    static void cancel(Context ctx) {
        AlarmManager aMgr = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);
        aMgr.cancel(createForwardingServiceIntent(ctx));
    }

    private static void scheduleSync(Context ctx, int inSeconds) {
        if (!StoredValues.addressIsSet(ctx)) {
            Log.d(Consts.TAG, "Not scheduling sync because no address is set.");
            return;
        }

        long atTime = System.currentTimeMillis() + inSeconds * 1000;
        PendingIntent pi = createForwardingServiceIntent(ctx);
        AlarmManager aMgr = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);
        aMgr.set(AlarmManager.RTC_WAKEUP, atTime, pi);
        Log.d(Consts.TAG, "Scheduled sync due in " + inSeconds + " seconds.");
    }

    private static PendingIntent createForwardingServiceIntent(Context ctx) {
        Intent serviceIntent = new Intent(ctx, ForwardingService.class);
        return PendingIntent.getService(ctx, 0, serviceIntent, 0);
    }

}
