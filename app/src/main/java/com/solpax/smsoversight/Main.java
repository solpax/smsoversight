package com.solpax.smsoversight;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;


public class Main extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        long lastSmsId = StoredValues.getMaxSyncedId(this);
        if(lastSmsId == 0){
            lastSmsId = getMaxSmsId();
            StoredValues.setMaxSyncedId(this,lastSmsId);
        }
    }

    private long getMaxSmsId() {
        ContentResolver r = getContentResolver();
        String selection = "type IN (1,2)";
        Cursor result = r.query(Uri.parse("content://sms"), null, selection, null,"_id DESC LIMIT 1");
        try
        {
            if (result.moveToFirst()) {
                return result.getLong(0);
            } else {
                return 0;
            }
        }
        catch (RuntimeException e)
        {
            result.close();
            throw e;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        String forwardAddress = StoredValues.getForwardingAddress(this);
        EditText edit = (EditText)findViewById(R.id.ForwardToAddress);
        edit.setText(forwardAddress);
        //edit.setEnabled(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void SaveClicked(View view){
        EditText edit = (EditText)findViewById(R.id.ForwardToAddress);
        String forwardAddress = edit.getText().toString();
        StoredValues.setForwardingAddress(this,forwardAddress);
    }

}
