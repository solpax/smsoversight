package com.solpax.smsoversight;

/**
 * application wide constants
 */
final  class Consts {
    /** TAG used for logging. */
    static final String TAG = "SmsOversight";
}
