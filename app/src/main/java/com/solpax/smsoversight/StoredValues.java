package com.solpax.smsoversight;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class StoredValues {

    //constants
    static final String LAST_SYNCED_SMS_ID = "last_synced_sms_id";
    static final String FORWARD_TO_ADDRESS = "foward_to_address";

    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static long getMaxSyncedId(Context ctx) {
        return getSharedPreferences(ctx).getLong(LAST_SYNCED_SMS_ID, 0);
    }

    public static void setMaxSyncedId(Context ctx, long lastId){
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putLong(LAST_SYNCED_SMS_ID, lastId);
        editor.commit();
    }

    public static String getForwardingAddress(Context ctx) {
        return getSharedPreferences(ctx).getString(FORWARD_TO_ADDRESS, null);
    }

    static void setForwardingAddress(Context ctx, String address) {
        Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(FORWARD_TO_ADDRESS, address);
        editor.commit();
    }

    public static boolean addressIsSet(Context ctx) {
        return (getForwardingAddress(ctx) != null);
    }
}
