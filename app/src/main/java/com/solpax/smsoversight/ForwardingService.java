package com.solpax.smsoversight;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.Process;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsManager;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class ForwardingService extends Service {

    private static boolean sIsRunning = false;

    private static WakeLock sWakeLock;

    private static HashMap<String,String> AddressNameMap = new HashMap<String, String>();

    private static String forwardToAddress = null;

    private final String SMSSENT = "sent_sms_oversight";
    private final String SMSID = "smsid";

    private int noticeId = 987654;

    private final BroadcastReceiver sendingBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (getResultCode())
            {
                case Activity.RESULT_OK:
                    long sentId = intent.getExtras().getLong(SMSID);
                    Log.i(Consts.TAG, "Sent " + String.valueOf(sentId) + " successfully");
                    long oldMaxId = StoredValues.getMaxSyncedId(context);
                    if(sentId > oldMaxId) StoredValues.setMaxSyncedId(context,sentId);
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    Log.i(Consts.TAG,"Send result generic failure");
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    Log.i(Consts.TAG,"Send result no service");
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    Log.i(Consts.TAG,"Send result null pdu");
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    Log.i(Consts.TAG,"Send result radio off");
                    break;
            }
        }
    };

    public ForwardingService() {
    }

    @Override
    public void onCreate(){
        this.registerReceiver(sendingBroadcastReceiver, new IntentFilter(SMSSENT));
    }

    @Override
    public void onStart(final Intent intent, int startId) {
        super.onStart(intent, startId);
        Log.d(Consts.TAG,"Starting service...");
        synchronized (this.getClass()) {
            // Only start a sync if there's no other sync going on at this time.
            if (!sIsRunning) {
                acquireWakeLock(this);
                sIsRunning = true;
                forwardToAddress = StoredValues.getForwardingAddress(this);
                // Start sync in new thread.
                new Thread() {
                    public void run() {
                        // Lower thread priority a little. We're not the UI.
                        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                        try {
                            if(forwardToAddress != null && forwardToAddress.trim() != "") {
                                //do backup
                                backup();
                            }
                        } catch (Exception e) {
                            Log.e(Consts.TAG,e.getMessage(), e);

                        } finally {
                            stopSelf();
                            Alarms.scheduleRegularSync(ForwardingService.this);
                            sIsRunning = false;
                            releaseWakeLock(ForwardingService.this);
                        }
                    }
                }.start();
            } else {
                Log.d(Consts.TAG, "ForwardingService.onStart(): Already running.");
            }
        }
    }

    @Override
    public void onDestroy(){
        this.unregisterReceiver(sendingBroadcastReceiver);
    }

    private void backup(){
        Log.i(Consts.TAG, "Starting backup...");
        String noticeMsg = "Forwarding messages to " + forwardToAddress;
        createNotification(noticeMsg,0,0);
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd yyyy hh:mm:ssa");
        Calendar calendar = Calendar.getInstance();
        Cursor cursor = getItemsToSync();
        if (cursor != null && cursor.getCount() > 0) {
            int msgCount = cursor.getCount();
            int curMsgNum = 1;
            while(cursor.moveToNext()){
                noticeMsg = "Forwarding msg " + String.valueOf(curMsgNum) + " of " + String.valueOf(msgCount) + " to " + forwardToAddress;
                createNotification(noticeMsg,curMsgNum,msgCount);
                long smsid = cursor.getLong(cursor.getColumnIndex("_id"));
                long smsdateLong = cursor.getLong(cursor.getColumnIndex("date"));
                calendar.setTimeInMillis(smsdateLong);
                String smsdate = formatter.format(calendar.getTime());
                String smsaddress = cursor.getString(cursor.getColumnIndex("address"));
                String smsbody = cursor.getString(cursor.getColumnIndex("body"));
                String smstype = "From";
                if(cursor.getLong(cursor.getColumnIndex("type")) == 2) {
                    smstype = "To";
                }
                Log.i(Consts.TAG, "Msg id " + String.valueOf(smsid) + " " + smstype + " " + smsaddress);
                //don't forward messages generated by forwarding
                //use "Contains" to allow for variants.
                if(!smsaddress.contains(forwardToAddress) && !forwardToAddress.contains(smsaddress)) {
                    String person = getName(smsaddress);
                    String msg = "SmsOversight " + smstype + " " + person + " @" + smsdate + "\n" + smsbody;
                    Intent sentIntent = new Intent(SMSSENT);
                    sentIntent.putExtra(SMSID,smsid);
                    PendingIntent sentPI  = PendingIntent.getBroadcast(this, 0, sentIntent , PendingIntent.FLAG_CANCEL_CURRENT);
                    SmsManager smsManager =  SmsManager.getDefault();
                    //this does not confirm delivery
                    try{
                        smsManager.sendTextMessage(forwardToAddress, null, msg, sentPI, null);
                    }catch(Exception x){
                        Log.e(Consts.TAG,x.getMessage(),x);
                    }
                }
                //StoredValues.setMaxSyncedId(this,smsid);
            }
        }else {
            Log.i(Consts.TAG,"Nothing to sync.");
        }
        cursor.close();
    }

    private void createNotification(String msg,int cur, int total){
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setContentText(msg);
        mBuilder.setContentTitle("SmsOversight");
        mBuilder.setSmallIcon(R.drawable.oversight);
        Intent resultIntent = new Intent(this,Main.class);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this,0, resultIntent,PendingIntent.FLAG_UPDATE_CURRENT );
        mBuilder.setContentIntent(resultPendingIntent);
        if(total > 0) {
            mBuilder.setProgress(cur,total,false);
        }
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notice = mBuilder.build();
        notice.flags |= Notification.FLAG_AUTO_CANCEL;
        mNotificationManager.notify(noticeId, notice);
    }

    private String getName(String address) {
        String name = null;
        if(AddressNameMap.containsKey(address)){
            name = AddressNameMap.get(address);
        }else{
            name = PersonLookup.getContactDisplayNameByNumber(this,address);
            AddressNameMap.put(address,name);
        }
        if(name == null) return address;
        return name;
    }

    private Cursor getItemsToSync() {
        long lastSmsId = StoredValues.getMaxSyncedId(this);
        if(lastSmsId <1) {
            Log.e(Consts.TAG,"Cannot determine last SMS Id");
            return null;
        }
        Log.i(Consts.TAG,"Last SMS id is: " + String.valueOf(lastSmsId));
        ContentResolver r = getContentResolver();
        String selection = "_id > ? AND type IN (1,2)";
        String[] selectionArgs = new String[] {
                String.valueOf(lastSmsId)
        };
        String sortOrder = "_id LIMIT 100";
        return r.query(Uri.parse("content://sms"), null, selection, selectionArgs, sortOrder);
    }

    private static void acquireWakeLock(Context ctx) {
        if (sWakeLock == null) {
            PowerManager pMgr = (PowerManager) ctx.getSystemService(POWER_SERVICE);
            sWakeLock = pMgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    "ForwardingService.sync() wakelock.");
        }
        sWakeLock.acquire();
    }

    private static void releaseWakeLock(Context ctx) {
        sWakeLock.release();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
